<?php 
// ver_dumpの省略を解除
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);
ini_set('xdebug.var_display_max_depth', -1);


// 判別元の配列
$array = [
	['id' => 1, 'name' => '最上位ノード', 	'parent_id' => 0, 'left' => 0, 'right' => 0],
	['id' => 2, 'name' => '子ノード１', 		'parent_id' => 1, 'left' => 0, 'right' => 0],
	['id' => 3, 'name' => '孫ノード１', 		'parent_id' => 2, 'left' => 0, 'right' => 0],
	['id' => 4, 'name' => '孫ノード２', 		'parent_id' => 2, 'left' => 0, 'right' => 0],
	['id' => 5, 'name' => 'ひ孫ノード', 		'parent_id' => 4, 'left' => 0, 'right' => 0],
	['id' => 6, 'name' => '孫ノード３', 		'parent_id' => 2, 'left' => 0, 'right' => 0],
	['id' => 7, 'name' => '子ノード２', 		'parent_id' => 1, 'left' => 0, 'right' => 0]
];




// 判別元の配列をツリー構造で表現
$new = array();
foreach ($array as $a){
    $new[$a['parent_id']][] = $a;
}
$tree = tree($new, array($array[0]));


// ツリー構造に即してleft rightの付与
$nodeId = 1;
culcTree($tree, $nodeId);

echo '<pre>';
print_r($tree);
echo '</pre>';


// 判別元の配列のアップデート
updateArray($tree, $array);

echo '<pre>';
print_r($array);
echo '</pre>';



function tree(&$list, $parent){
    $tree = array();
    foreach ($parent as $key=>$value){
        if(isset($list[$value['id']])){
			$value['nodeCount'] = 0;
			$value['children'] = tree($list, $list[$value['id']]);
        }else{
			$value['nodeCount'] = 0;
        	$value['children'] = false;
        }
        $tree[] = $value;
    } 
    return $tree;
}


function culcTree(&$list, &$nodeId){
	foreach($list as $k => $v){
		if(!empty($v['children'])){
			$nodeCount = count($list[$k]['children'], COUNT_RECURSIVE) / 8;

			$list[$k]['nodeCount'] = $nodeCount;
			$list[$k]['left'] = $nodeId;

			$nodeId += 1;

			$list[$k]['right'] = $nodeId + $nodeCount * 2;

			culcTree($list[$k]['children'], $nodeId);

		}else{
			$list[$k]['nodeCount'] = 0;
			$list[$k]['left'] = $nodeId;

			$nodeId += 1;

			$list[$k]['right'] = $nodeId;
		}

		$nodeId = $list[$k]['right'] + 1;


	}
}


function updateArray($tree, &$array){
	foreach($tree as $k => $v){

		foreach($array as $key => $value){

			if($v['id'] == $value['id']){
				$array[$key]['left'] = $v['left'];
				$array[$key]['right'] = $v['right'];
				break;
			}
		}

		if(!empty($v['children'])){
			updateArray($v['children'], $array);
		}
	}
}




